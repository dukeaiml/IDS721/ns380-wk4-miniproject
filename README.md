# Individual Project 2

This project creates a rust webservice using the Actix-Web framework to serve 2 endpoints, call and get_calls. This service effectively works as a cache, only
performing the calculation if it has not been performed before, and recording the number of times the same call has been requested.

A demonstration of this application is available here: https://youtu.be/VjD7NnvM0C0

## Build

This project can be built and tested using `cargo build` or `cargo run`. However, this is not the intended usage of the application. The intended use is utilizing Docker to containerize and serve the application. This workflow is expanded upon in the `Usage` section of this README.

This project also has CI/CD set up to automatically build and verify that the Docker container successfully builds on every push. You can see the configuration for this pipeline in the `.gitlab-ci.yml` file.

## Usage

There are a few steps to using this service. From the root of this repository, run
```
docker build -t rust_actix .
```
This will build a docker container that you can then run with a command like
```
docker run -it --rm -p 8080:8080 rust_actix
```
This will use up the terminal, so in a new terminal, run the command
```
curl -X POST -H "Content-Type: application/json" --data '{"input": "1 + 2"}' http://localhost:8080/call
```
You can replace the string in the input with some other mathematical equations. Once you have done a few, call the other endpoint like
```
curl -X GET http://localhost:8080/get_calls
```
This will return the number of times you have passed each input. An example of the usage is shown below:

![alt text](image.png)