use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder, http::header::ContentType};
use serde::{Deserialize, Serialize};
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
pub struct CallData {
    pub input: String,
}

lazy_static::lazy_static! {
    static ref DATA_MAP: Arc<Mutex<HashMap<String, (f64, i32)>>> = Arc::new(Mutex::new(HashMap::new()));
}

#[get("/get_calls")]
async fn get_calls() -> impl Responder {
    println!("Got get_calls request");
    let mut out: String = "".to_owned();
    let map = DATA_MAP.lock().unwrap();
    for (query, res) in &*map {
        let count = res.1;
        out.push_str(&*format!("{query}: {count}\n"));
    }

    HttpResponse::Ok().content_type(ContentType::plaintext()).body(out)
}

#[post("/call")]
async fn call(data: web::Json<CallData>) -> impl Responder {
    println!("Got call request");
    let math_operation = &data.input;

    let mut map = DATA_MAP.lock().unwrap();
    if map.contains_key(math_operation) {
        let k = *map.get_mut(math_operation).unwrap();
        map.insert(data.input.clone(), (k.0, k.1+1));
        let res = k.0;
        return HttpResponse::Ok().content_type(ContentType::plaintext()).body(format!("Result: {res}\n"));
    }

    // Evaluate the mathematical expression
    let result = meval::eval_str(math_operation).unwrap();

    map.insert(data.input.clone(), (result.clone(), 1));

    // Return the result as a string
    HttpResponse::Ok().content_type(ContentType::plaintext()).body(format!("Result: {result}\n"))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(call).service(get_calls))
        .bind(("0.0.0.0", 8080))?
        .run()
        .await
}