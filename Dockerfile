# Use the official Rust image as the build environment
FROM rust:slim-buster as builder

# Set the working directory
WORKDIR /app

# Copy the source code into the container
ADD . ./

# Build the application
RUN cargo clean && cargo build -vv --release

# Create a new lightweight container for the application
FROM debian:buster-slim

# Set the working directory
WORKDIR /app

# Copy the compiled binary from the builder stage into the lightweight container
COPY --from=builder /app/target/release/cache_service .

# Expose the port your application listens on
EXPOSE 8080

# Command to run the application
CMD ["./cache_service"]
